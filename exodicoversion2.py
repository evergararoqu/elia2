import sys
#DEFINITION DES FONCTIONS VERSION 2
def menu():
    print("""Programme de molécules:,
    Que voulez vous faire?,
    Insérez 0 pour quitter,
    Insérez 1 pour saisir des noms, poids et s&quence ADN,
    Insérez 2 pour afficher les molécules, leurs poids respectifs, et leurs séquence ADN,
    Insérez 3 pour calculer la moyenne,
    Insérez 4 pour afficher la molécule qui possède le poids max,
    Insérez 5 pour afficher les molecules avec un poids superieur a la moyenne""")

def nompoidsseqmlc():
    print("Combien de molécules?")
    nb=int(input())
    for i in range (nb):
        mol={}
        print("Donnez le nom de la molécule",i+1)
        nom=input()
        print("Donnez le poids de la molécule",i+1)
        poids=int(input())
        print("Donnez la séquence ADN des molécules")
        seq=input()
        mol["poids"]=poids
        mol["sequence"]=seq
        dico[nom]=mol
    return dico
    
def affiche(dico):
    print("Affichage des molécules et leurs séquences et poids respectifs")
    for cle in dico.keys():
        print("la molécule",cle,"a le poids",dico[cle]["poids"],"avec la sequence",dico[cle]["sequence"])

def moyenne(dico):
    somme=0
    for cle  in dico.keys():
        somme=somme+dico[cle]["poids"]
    moy=somme/len(dico)
    print("La moyenne est",moy)
    return moy
def poidsmax(dico):
    max=0
    for cle in dico.keys():
        if dico[cle]["poids"]>max:
            max=dico[cle]["poids"]
    print (max,"est le poids max de tous les poids")

def supmoy(dico,moy):
    for cle in dico.keys():
        if dico[cle]["poids"]>moy:
            print("La molécule",cle,"a un poids supérieur à la moyenne")


#PROGRAMME PRINCIPAL VERISON 2
dico={}
moy=0
c=1
while c>0:
    menu()
    c=int(input())
    if c==0:
        print("Fermeture du programme")
        sys.exit()
    if c==1:
        nompoidsseqmlc()
    if c==2:
        affiche(dico)
    if c==3:
        moy=moyenne(dico)
    if c==4:
        poidsmax(dico)
    if c==5:
        supmoy(dico,moy)
